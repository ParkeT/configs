set nocompatible               " be iMproved
filetype off                   " required!

call vundle#rc()
filetype indent on     " required! 
Bundle "https://github.com/Lokaltog/vim-distinguished.git"
Bundle "jelera/vim-javascript-syntax"
Bundle "scrooloose/syntastic"
Bundle 'scrooloose/nerdtree'
Bundle 'Yggdroot/indentLine'
set t_Co=256
syntax on
"set background=dark
colorscheme distinguished
"let g:syntastic_check_on_open=1
" show existing tab with 4 spaces width
set tabstop=4
" " when indenting with '>', use 4 spaces width
set shiftwidth=4
" " On pressing tab, insert 4 spaces
set expandtab
set number
set relativenumber
set mouse=nvicr

" This does what it says on the tin. It will check your file on open too, not just on save.
" You might not want this, so just leave it out if you don't.
let g:syntastic_check_on_open=1
let g:syntastic_javascript_checkers=['eslint']

set showcmd
autocmd BufNewFile,BufRead *.json set ft=javascript
autocmd BufWritePre *.js normal m`:%s/\s\+$//e`` 
let g:indentLine_char = '¦'
let g:indentLine_color_term = 65 
let g:indentLine_color_gui = '#5f875f'
let g:indentLine_enabled = 1
set formatoptions-=c
set formatoptions-=r 
set formatoptions-=o
set completeopt=longest,menu

 "set cursorline
set nowrapscan
set wildmode=longest,list
set cursorline
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
set undodir=~/.vim/undo//
set hidden
